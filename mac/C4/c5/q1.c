#include <stdio.h>
#include <stdlib.h>

struct cell {
    int data;
    struct cell *next;
};

struct cell *add(struct cell *list, int data) {
    struct cell *arr, *p;
    arr = (struct cell *)malloc(sizeof(struct cell));
    arr -> data = data;
    arr -> next = NULL;
    if(list == NULL) {
        return arr;
    } else {
        p = list;
        while(p -> next) {
            p = p -> next;
        }
        p -> next = arr;
        return list;
    }
}

struct cell *arrayToList(int array[], int n) {
    struct cell *list = NULL;
    for(int i = 0; i < n; i++) {
        list = add(list, array[i]);
    }
    return list;
}

void print(struct cell *list) {
    printf("[ ");
    while(list) {
        printf("%d ", list -> data);
        list = list -> next;
    }
    printf(" ]\n");
}

int main(void) { 
    int a1[ ] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    print( arrayToList( a1, 9 ) );
    print( arrayToList( a1, 0 ) );
    return 0;
} 