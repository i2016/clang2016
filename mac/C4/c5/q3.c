#include <stdio.h>
#include <stdlib.h>

typedef struct cell {
    int data;
    struct cell *next;
}Cell, *List, *Element;

List add(List list, int data) {
    struct cell *arr, *p;
    arr = (List)malloc(sizeof(Cell));
    arr -> data = data;
    arr -> next = NULL;
    if(list == NULL) {
        return arr;
    } else {
        p = list;
        while(p -> next) {
            p = p -> next;
        }
        p -> next = arr;
        return list;
    }
}


List delete(List list, List elm) {
    List p;
    List prev = NULL;
    p = list;
    while(p != elm) {
        prev = p;
        p = p -> next;
    }
    if(prev) {
        prev -> next = elm -> next;
    } else { 
        list = elm -> next;
    }
    return list;
}

List deleteEven(List list) {
    Element p;
    Element next;
    p = list;
    while(p) {
        next = p -> next;
        if(p -> data%2 == 0) {
            list = delete(list, p);
        }
        p = next;
    }
    return list;
}

List deleteAll(List list) {
    while(list) {
        list = delete(list, list);
    }
    return NULL;
}

List arrayToList(int array[], int n) {
    List list = NULL;
    for(int i = 0; i < n; i++) {
        list = add(list, array[i]);
    }
    return list;
}

int length(List list) {
    int len = 0;
    while(list) {
        len += 1;
        list = list -> next;
    }
    return len;
}

void print(List list) {
    printf("[ ");
    while(list) {
        printf("%d ", list -> data);
        list = list -> next;
    }
    printf("]\n");
}

void test(int array[], int n) {
    struct cell * list;
    list = arrayToList(array, n);
    printf("初期リスト:長さ:%d リスト:", length(list));
    print(list);
    list = deleteEven(list);
    printf("偶数削除後:長さ:%d リスト:", length(list));
    print(list);
    list = deleteAll(list);
    printf("全削除後: 長さ:%d リスト:", length(list));
    print(list);
    printf("\n");
}


int main(void) {
    int a1[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    int a2[] = { 2 };
    int a3[] = { 34567 };
    int a4[] = { 154, -871, 247, 0, 116, -1,  -3454 };
    test(a1, 9);
    test(a1, 0);
    test(a2, 1);
    test(a3, 1);
    test(a4, 7);
    return 0;
}