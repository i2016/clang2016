#include <stdio.h>
#include <stdlib.h>

struct cell {
    int data;
    struct cell *next;
};

struct cell *add(struct cell *list, int data) {
    struct cell *arr, *p;
    arr = (struct cell *)malloc(sizeof(struct cell));
    arr -> data = data;
    arr -> next = NULL;
    if(list == NULL) {
        return arr;
    } else {
        p = list;
        while(p -> next) {
            p = p -> next;
        }
        p -> next = arr;
        return list;
    }
}


struct cell *delete(struct cell *list, struct cell *elm) {
    struct cell *p;
    struct cell *prev = NULL;
    p = list;
    while(p != elm) {
        prev = p;
        p = p -> next;
    }
    if(prev) {
        prev -> next = elm -> next;
    } else { 
        list = elm -> next;
    }
    return list;
}

struct cell *deleteEven(struct cell *list) {
    struct cell *p;
    struct cell *next;
    p = list;
    while(p) {
        next = p -> next;
        if(p -> data%2 == 0) {
            list = delete(list, p);
        }
        p = next;
    }
    return list;
}

struct cell *deleteAll(struct cell *list) {
    while(list) {
        list = delete(list, list);
    }
    return NULL;
}

struct cell *arrayToList(int array[], int n) {
    struct cell *list = NULL;
    for(int i = 0; i < n; i++) {
        list = add(list, array[i]);
    }
    return list;
}

int length(struct cell *list) {
    int len = 0;
    while(list) {
        len += 1;
        list = list -> next;
    }
    return len;
}

void print(struct cell *list) {
    printf("[ ");
    while(list) {
        printf("%d ", list -> data);
        list = list -> next;
    }
    printf("]\n");
}

void test(int array[], int n) {
    struct cell * list;
    list = arrayToList(array, n);
    printf("初期リスト:長さ:%d リスト:", length(list));
    print(list);
    list = deleteEven(list);
    printf("偶数削除後:長さ:%d リスト:", length(list));
    print(list);
    list = deleteAll(list);
    printf("全削除後: 長さ:%d リスト:", length(list));
    print(list);
    printf("\n");
}


int main(void) {
    int a1[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    int a2[] = { 2 };
    int a3[] = { 34567 };
    int a4[] = { 154, -871, 247, 0, 116, -1,  -3454 };
    test(a1, 9);
    test(a1, 0);
    test(a2, 1);
    test(a3, 1);
    test(a4, 7);
    return 0;
}