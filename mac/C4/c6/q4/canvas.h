#define X 50
#define Y 50

typedef char Canvas[X][Y];
void clear(Canvas c);
void mark(Canvas c, int x, int y);
void show(Canvas c);