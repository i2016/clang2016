#include <stdio.h>
#include "canvas.h"

void clear(Canvas c) {
    int i, j;
    for (i = 0; i < X; i++) {
        for (j = 0; j < Y; j++) {
            c[i][j] = ' '; 
        }
    }
}

void mark(Canvas c, int x, int y) {
    if (x < 0 || x >= X || y < 0 || y >= Y) return; 
    c[x][y] = '*';
}

void show(Canvas c) {
    int i, j;
    for (j = Y - 1; j >= 0; j--) {
        for (i = 0; i < X; i++) {
            putchar(c[i][j]);
        }
        putchar('\n');
    }
}