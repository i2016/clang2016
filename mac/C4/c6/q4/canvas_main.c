#include <stdio.h>
#include "canvas.h"

int f1(int x) { return x / 3; }
int f2(int x) { return -x + 30; }
int f3(int x) { return x * x / 20 - x / 2 + 4; }
int f4(int x) { return (x*x*x - 60 * x*x + 900 * x) / 200; }

void draw_f1(Canvas c) {
	int i;
	clear(c);
	for (i = 0; i < X; i++) {
		mark(c, i, f1(i));
	}
	show(c);
}

void draw_f2(Canvas c) {
	int i;
	clear(c);
	for (i = 0; i < X; i++) {
		mark(c, i, f2(i));
	}
	show(c);
}

void draw_f3(Canvas c) {
	int i;
	clear(c);
	for (i = 0; i < X; i++) {
		mark(c, i, f3(i));
	}
	show(c);
}

void draw_f4(Canvas c) {
	int i;
	clear(c);
	for (i = 0; i < X; i++) {
		mark(c, i, f4(i));
	}
	show(c);
}

int main(void) {
	Canvas c;

	draw_f1(c);
	getchar();

	draw_f2(c);
	getchar();

	draw_f3(c);
	getchar();

	draw_f4(c);
}