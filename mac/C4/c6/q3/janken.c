#include <stdio.h>
#include <stdbool.h>  // true
#include <stdlib.h>   // srand rand
#include <time.h>     // time
#include <string.h> 
#include "counter.h"
#define BUFLEN 256

int main(void) {
	int comHand, manHand;
	char input[BUFLEN];
	int judge[3][3] = { { 0, 1, -1 }, { -1, 0, 1 }, { 1, -1, 0 } };
	char* hand[] = { "グー", "チョキ", "パー" };
	Counter round, manTotal, comTotal, drawTotal;

	printf("コンピュータとのじゃんけん対戦ゲーム\n");

	srand((unsigned) time(NULL));

	init(&round);
    init(&manTotal);
    init(&comTotal);
    init(&drawTotal);

	while (true) {
		inc(&round);
		printf("\n 第%d 回目のゲーム\n", get(&round));

		do {
			printf("何を出しますか?(1:グー 2:チョキ 3:パー):");
			fgets(input, BUFLEN, stdin);
		} while (input[0] != '1' && input[0] != '2' && input[0] != '3');
		manHand = input[0] - '1';
		printf(" あなた: %s\n", hand[manHand]);

		comHand = rand() % 3;
		printf(" コンピュータ:%s\n", hand[comHand]);

		if (judge[comHand][manHand]>0) {
			printf(" コンピュータの勝ち\n");
			inc(&comTotal);
		} else if (judge[comHand][manHand]<0) {
			printf(" あなたの勝ち\n");
			inc(&manTotal);
		} else {
			printf(" 引き分け\n");
			inc(&drawTotal);
		}
		printf("あなたの %d 勝 %d 敗 %d 引き分け です.\n",get(&manTotal), get(&comTotal), get(&drawTotal));

		do {
			printf("続けますか?(y/n) ");
			fgets(input, BUFLEN, stdin);
		} while (input[0] != 'y' && input[0] != 'Y' && input[0] != 'n' && input[0] != 'N');
		if (input[0] == 'n' || input[0] == 'N') {
			printf("終了します\n");
			break;
		}
	}
}
