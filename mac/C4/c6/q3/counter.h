typedef int Counter;
void init(Counter* c);
void inc(Counter* c);
int get(Counter* c);