#include "counter.h"
void init(Counter* c) { 
    *c = 0; 
}
void inc(Counter* c) { 
    *c += 1; 
}
int get(Counter* c){ 
    return *c;
}
