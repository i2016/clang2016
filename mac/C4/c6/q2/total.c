static int comTotal, manTotal, drawTotal;
void initComTotal() { comTotal = 0; } void incComTotal() { comTotal++; }
int getComTotal() { return comTotal; }
void initManTotal() { manTotal = 0; } void incManTotal() { manTotal++; }
int getManTotal() { return manTotal; }
void initDrawTotal() { drawTotal = 0; } void incDrawTotal() { drawTotal++; }
int getDrawTotal() { return drawTotal; }