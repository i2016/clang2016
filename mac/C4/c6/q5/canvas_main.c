#include <stdio.h>
#include "canvas.h"

int f1(int x) { return x / 3; }
int f2(int x) { return -x + 30; }
int f3(int x) { return x * x / 20 - x / 2 + 4; }
int f4(int x) { return (x*x*x - 60 * x*x + 900 * x) / 200; }

void draw_f(Canvas c, int (*f)(int)) {
	int i;
	clear(c);
	for (i = 0; i < X; i++) {
		mark(c, i, (*f)(i));
	}
	show(c);
}

int main(void) {
	Canvas c;

	draw_f(c, &f1);
	getchar();

	draw_f(c, &f2);
	getchar();

	draw_f(c, &f3);
	getchar();

	draw_f(c, &f1);
	getchar();
}