#include <stdio.h>
#include <stdbool.h>  // true
#include <stdlib.h>   // srand rand
#include <time.h>     // time
#include <string.h> 
#include "total.h"
#include "round.h"
#define BUFLEN 256

int main(void) {
	int comHand, manHand;
	int round = 0;
	char input[BUFLEN];
	int judge[3][3] = { { 0, 1, -1 }, { -1, 0, 1 }, { 1, -1, 0 } };
	char* hand[] = { "グー", "チョキ", "パー" };

	printf("コンピュータとのじゃんけん対戦ゲーム\n");

	srand((unsigned) time(NULL));

	initRound();
	initManTotal();
	initComTotal();
    initDrawTotal();

	while (true) {
		incRound();
		printf("\n 第%d 回目のゲーム\n", getRound());

		do {
			printf("何を出しますか?(1:グー 2:チョキ 3:パー):");
			fgets(input, BUFLEN, stdin);
		} while (input[0] != '1' && input[0] != '2' && input[0] != '3');
		manHand = input[0] - '1';
		printf(" あなた: %s\n", hand[manHand]);

		comHand = rand() % 3;
		printf(" コンピュータ:%s\n", hand[comHand]);

		if (judge[comHand][manHand]>0) {
			printf(" コンピュータの勝ち\n");
            incComTotal();
		} else if (judge[comHand][manHand]<0) {
			printf(" あなたの勝ち\n");
            incManTotal();
		} else {
			printf(" 引き分け\n");
			incDrawTotal();
		}
		printf("あなたの %d 勝 %d 敗 %d 引き分け です.\n",getManTotal(), getComTotal(), getDrawTotal());

		do {
			printf("続けますか?(y/n) ");
			fgets(input, BUFLEN, stdin);
		} while (input[0] != 'y' && input[0] != 'Y' && input[0] != 'n' && input[0] != 'N');
		if (input[0] == 'n' || input[0] == 'N') {
			printf("終了します\n");
			break;
		}
	}
}
