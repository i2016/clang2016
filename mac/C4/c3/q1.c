#include <stdio.h>

enum color {White, Black, Red, Blue, Green};

int main(void) {
    enum color x;
    printf("整数値(0~4)を入力:"); 
    scanf("%d",&x);
    switch(x){
        case White: puts("->「白」です.");break;
        case Black: puts("->「黒」です.");break;
        case Red: puts("->「赤」です.");break;
        case Blue: puts("->「青」です.");break;
        case Green:  puts("->「緑」です.");break;
        default: puts("->入力値が不正です.");
    }
    return 0;
}