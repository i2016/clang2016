#include <stdio.h>
// #define N 8
#define N 200000

void merge(int A[], int p, int q, int r) {
    int B[N];
    int i,j,k;
    if(p==r) return;
    for(i=p; i<=r; i++) B[i] = A[i];
    i = p;
    j = q+1;
    k = p;
    while(i<=q && j<=r) {
        if(B[i]<B[j]) A[k++] = B[i++];
        else A[k++] = B[j++];
    }
    if(i>q) while(k<=r) A[k++] = B[j++];
    else if(j>r) while(k<=r) A[k++] = B[i++];
}

void mergeSort(int A[], int p, int r) { 
    int q;
    if(p < r){
       q = (p+r)/2;
       mergeSort(A, p, q);
       mergeSort(A, q+1, r);
       merge(A, p, q, r);
    }
}

int main(void) {
    int i,A[N] = {5, 2, 4, 6, 1, 3, 2, 6};
    printf("Before A[] = ");
    for(i=0; i<N; i++) printf("%d ", A[i]);
    printf("\n");
    mergeSort(A, 0, N-1);
    printf("After A[] = ");
    for(i=0; i<N; i++) printf("%d ", A[i]);
    printf("\n");
    return 0;
}

