#include <stdio.h>
#include <stdlib.h>
#define MAX 81

int param_test(int p1, int p2) {
    printf("p1=%d, p2=%d で関数が実行されました.\n", p1, p2);
    return 0;
}

int main(void) {
    char buffer[MAX];
    int p1, p2;
    FILE *fi;
    if((fi = fopen("params.dat", "r")) == NULL) {
        printf("Error!\n");
        exit(1); 
    }
    while(fgets(buffer, MAX, fi)) {
        if(buffer[0] != '#') {
            sscanf(buffer, "%d%d", &p1, &p2);
            param_test(p1, p2);
        }
    }
    fclose(fi);
    return 0;
}