#include <stdio.h>
#include <math.h>

void solve_quad_eq(int a, int b, int c) {
    double x1, x2, D;
    D = b*b - 4*a*c;
    if(D>0) {
        x1 = (-b+sqrt(D)) / (2.0*a);
        x2 = (-b-sqrt(D)) / (2.0*a);
        printf("x = %.1f, x = %.1f\n", x1, x2);
    }
    else if(D==0) {
        x1 = -b/(2.0*a);
        printf("x = %.1f\n", x1);
    }
    else {
        printf("解なし\n");
    }

}
int main(void) {
    int a, b, c;
    printf("a? ");
    scanf("%d", &a);
    printf("b? ");
    scanf("%d", &b);
    printf("c? ");
    scanf("%d", &c);
    solve_quad_eq(a, b, c);
    return 0;
}