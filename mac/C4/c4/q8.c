#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 81

void solve_quad_eq(int a, int b, int c) {
    double x1, x2, D;
    D = b*b - 4*a*c;
    if(D>0) {
        x1 = (-b+sqrt(D)) / (2.0*a);
        x2 = (-b-sqrt(D)) / (2.0*a);
        printf("x = %.1f, x = %.1f\n", x1, x2);
    }
    else if(D==0) {
        x1 = -b/(2.0*a);
        printf("x = %.1f\n", x1);
    }
    else {
        printf("解なし\n");
    }

}
int main(void) {
    int a, b, c, n;
    char buffer[MAX];
    FILE *fi;
    if((fi = fopen("coefs.dat", "r")) == NULL) {
        printf("Error!\n");
        exit(1); 
    }
    printf("2次方程式の実数解を表示します:¥n");
    while(fgets(buffer, MAX, fi)) {
        if(buffer[0] != '#') {
            sscanf(buffer, "%d%d%d", &a, &b, &c);
            printf("%d: ", n++); 
            solve_quad_eq(a, b, c);
        }
    }
    fclose(fi);
    return 0;
}