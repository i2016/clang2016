#include <stdio.h>
#include <stdlib.h>
#define MAX 81

int main(void) {
    char buffer[MAX];
    FILE *fi;
    if((fi = fopen("params.dat", "r")) == NULL) {
        printf("Error!\n");
        exit(1); 
    }
    printf("Parameter settings (p1, p2): \n");
    while(fgets(buffer, MAX, fi)) {
        if(buffer[0] != '#') {
            printf("%s", buffer);
        }
    }
    fclose(fi);
    return 0;
}