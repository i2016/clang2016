#include <stdio.h>

int main(void) {
    float h, w, n=0, have=0, wave=0;
    FILE *fi;
    if((fi = fopen("kensa.dat", "r"))==NULL) {
        printf("Error!\n");
    }
    else { 
        while(fscanf(fi, "%f%f", &h, &w) != EOF) {
            have += h;
            wave += w;
            n++;
            printf("No.%3.0f%7.1f%7.1f\n", n, h, w);
        }
        printf("\n");
        printf("\n 合 計%7.1f%7.1f\n", have, wave);
        printf("人 数%7.0f%7.0f\n", n, n);
        printf("平均値%7.1f%7.1f\n", have/n, wave/n); 
        fclose(fi);
    }
    return 0;
}