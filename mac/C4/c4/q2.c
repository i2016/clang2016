#include <stdio.h>

int main(void) {
    float h, w;
    int n = 1;
    FILE *fi;
    fi = fopen("kensa.dat", "a");
    if(fi==NULL){
        printf("出力ファイルの準備ができていません.\n");
    }
    else {
        printf("身長 体重の順にデータを入力してください\n");
        printf("%d 件目-> ", n);
        while(scanf("%f%f", &h, &w) != EOF) {
            fprintf(fi, "%.1f %.1f\n", h, w);
            printf("%d 件目-> ", ++n);
        }
    fclose(fi);
    }
    return 0;
    }