#include <stdio.h>

int main(void) {
    float h=180.0, w=72.0;
    FILE *fi;
    fi = fopen("kensa.dat", "w");
    if(fi==NULL){
        printf("出力ファイルの準備ができていません.\n");
    }
    else {
        fprintf(fi, "%.1f %.1f\n", h, w);
        printf("1 件のデータを書き込みました:\n");
    }
    printf("身長:%6.1f 体重:%6.1f\n",h,w);
    fclose(fi);
    return 0;
    }