#include <stdio.h>
#include <stdlib.h>
#define N 21
#define M 41

int main(void) {
    int no=0;
    char name[N], address[M], tel[N];
    FILE *fi, *fo;
    if((fo = fopen("meibo.dat", "w")) == NULL) { 
        printf("Error\n");
        exit(1);
    } 
    printf("番号:%d\n", ++no);
    printf("氏名? ");
    while(scanf("%s", name) != EOF) {
        printf("現住所? "); 
        scanf("%s", address);
        printf("電話番号? ");
        scanf("%s", tel);
        fprintf(fo, "%3d%20s%40s%20s\n", no, name, address, tel);
        printf("番号:%d\n", ++no);
        printf("氏名? ");
    }
    fclose(fo);
    if((fi = fopen("meibo.dat", "r")) == NULL) {
        printf("Error\n");
        exit(1);
    }
    printf("--- 入力データ ---\n");
    while(fscanf(fi, "%d%s%s%s", &no, name, address, tel) != EOF) {
        printf("番号: %d\n", no); 
        printf("氏名: %s\n", name);
        printf("住所: %s\n", address);
        printf("電話: %s\n", tel);
        printf("-----------\n");
    }
    fclose(fi);
    return 0;
}