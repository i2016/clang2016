#include <stdio.h>
int main(void) {
    char a[]="abcd",*p="abcd";
    
    puts("(1) addressの表示");
    printf("配列 a の先頭アドレス:%p\n", a);
    printf("ポインタ p の指すアドレス:%p\n", p);
    printf("文字列リテラル\"abcd\" :%p\n", "abcd");

    puts("(2) 文字配列 a の表示");
    printf("a の内容 :%s\n", a);
    a[0] = '@';
    printf("更新後の内容:%s\n", a);
    
    puts("(3) pointer p の指す文字列の表示");
    printf("pの指す内容 :%s\n",p);
    p[0] = '@';
    printf("更新後の内容:%s\n", p);

    return 0;
}
