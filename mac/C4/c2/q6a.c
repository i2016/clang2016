#include <stdio.h>
// #define N 5
int main(void) {
    char *strtbl[] = { "apple","banana","lemon","melon","orange" };
    int i;
    
    for(i=0; i<sizeof(strtbl); i++) {
        printf("%d: %s\n", i, strtbl[i]);
    }

    return 0;
}