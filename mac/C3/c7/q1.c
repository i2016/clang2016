#include <stdio.h>
#include <stdlib.h>
int main(void) {
    double h= 180.0, w= 72.0, a, b;
    FILE *fp;

    if ((fp = fopen("kensa.dat","w")) == NULL) {
        printf("file open error\n");
        exit(1);
    } else {
        fprintf(fp, "%f %f\n", h, w);
    }
    fclose(fp);

    if((fp = fopen("kensa.dat", "r")) == NULL) {
        printf("file open error\n");
        exit(1);
    } else {
        fscanf(fp, "%lf %lf\n ", &h, &w);
        printf("身長:%6.1f 体重:%6.1f\n",h,w);
    }
    fclose(fp);
    return 0;
}