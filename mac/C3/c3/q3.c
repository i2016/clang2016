#include <stdio.h>
#include <math.h>
struct triangle {
    double a,b,c;
};

int main(void) {
    struct triangle hen;
    double p,s;
    printf("a? ");
    scanf("%lf", &hen.a);
    printf("b? ");
    scanf("%lf", &hen.b);
    printf("c? ");
    scanf("%lf", &hen.c);
    p=(hen.a+hen.b+hen.c)/2;
    s=p*(p-hen.a)*(p-hen.b)*(p-hen.c);
    if(s < 0) puts("そのような三角形は存在しません.");
    else{
        printf("三角形の面積は%.1f です.\n", sqrt(s));
    }
    return 0;
}