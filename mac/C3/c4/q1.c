#include <stdio.h>
typedef struct point {
    double cx;
    double cy;
} Point;
Point set_point(double x, double y) {
    Point pt;
    pt.cx = x;
    pt.cy = y;
    return pt;
}

int main(void) {
    Point pt;
    printf("座標：(%4.1f, %4.1f)\n", pt.cx, pt.cy);
    return 0;
}