#include  <stdio.h>
typedef struct{
    double cx;
    double cy;
} Point;
typedef struct{
    Point pt;
    double r;
} Circle;
int is_inside(Circle *c, Point *pt);

int main(void){
    Circle c;
    Point pt;
    printf("中心点のx座標："); 
    scanf("%lf", &c.pt.cx);
    printf("中心点のy座標："); scanf("%lf", &c.pt.cy);
    printf("半径: "); 
    scanf("%lf", &c.r);
    printf("点のx座標: "); 
    scanf("%lf", &pt.cx);
    printf("点のy座標: "); scanf("%lf", &pt.cy);
    if(is_inside(&c, &pt)) printf("点Ａは円の内部に含まれる．\n");
    else printf("点Ａは円の内部に含まれない．\n");
    return 0;
}

int is_inside(Circle *c, Point *pt){
    double dist2 = 0;
    dist2 += (c->pt.cx - pt->cx)*(c->pt.cx - pt->cx);
    dist2 += (c->pt.cy - pt->cy)*(c->pt.cy - pt->cy);
    if(dist2<=c->r*c->r) return 1;
    else return 0;
}