#include <stdio.h>
int main(void) {
    int get, count=0;
    while((get = getchar()) != EOF) {
        if(get >= 'A' && get <= 'Z') count++;
        else if(get == '\n') {
            printf("大文字の個数は%d\n",count);
            count = 0;
        }
    }
    puts("プログラムを終了します。\n");
    return 0;
}