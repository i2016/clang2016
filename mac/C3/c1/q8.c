#include <stdio.h>
char test[3] = "abc";
int main(void) {
    int i,get,count =0 ;
    do{
        printf("練習を開始します\n");
        for(i=0;i<3;i++) {
            if(test[i] == '\0') break;
            printf("%c\n",test[i]);
            get = getchar();
            getchar();
            if(get != test[i]) printf("間違えました！\n");
            else {
                printf("正解です！\n");
                count++;
            }
        }
        printf("あなたの得点は%d点です\n",count);
        printf("継続しますか？ Y/N\n");
        get = getchar();
        getchar();
    }while(get != 'N');
    puts("終了します。");
    return 0;
}