#include <stdio.h>
#define Arr 10
#define ANS 0.78
int main(void) {
    int i;
    double array[Arr];
    for(i=0;i<Arr;i++) {
        array[i] = ANS*i;
    }
    for(i = 0; i < Arr; i++) {
        printf("%f\n", array[i]);
    }
    return 0;
}