#include<stdio.h>

int mycopy(char str[], char *p);
int main(void) {
	char a[80]="Empty",*b="Infomation";
	printf("代入前:%s\n", a);
	mycopy(a, b);
	printf("代入後:%s\n", a);
	return 0;
}

int mycopy(char str[], char *p) {
	int ilength = 0;
	while (*(p + ilength) != NULL) {
		str[ilength] = *(p + ilength);
		ilength++;
	}
	str[ilength] = '\0';
	return ilength;
}