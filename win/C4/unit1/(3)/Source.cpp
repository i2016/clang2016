#include<stdio.h>
#include<math.h>
#define MAXARRAY 100

struct point {
	double x, y;
};

double distance(struct point *p1, const struct point *p2) {
	double disx = p1->x - p2->x;
	double disy = p1->y - p2->y;
	return sqrt(disx*disx + disy*disy);
}

double maxdistance( struct point p[], int n) {
	double nowmax = 0.0,nowval;
	int i, j;
	int idx1, idx2;

	idx1 = idx2 = 0;

	for (i = 0; i < n - 1; i++) {
		for (j = i + 1; j < n; j++) {
			nowval = distance(&p[i],&p[j]);
			if (nowval > nowmax) {
				nowmax = nowval;
				idx1 = i;
				idx2 = j;
			}
		}
	}
	printf("最遠点:(%f,%f)-(%f,%f)\n", p[idx1].x, p[idx1].y, p[idx2].x, p[idx2].y);
	return nowmax;
}

int main(void) {
	struct point ary[MAXARRAY];
	int n, i;
	do {
		printf("座標数:"); scanf("%d", &n);
		if (n < 2)
			printf("座標が小さい\n");
		else if (n>MAXARRAY)
			printf("座標数が多い\n");
	} while (n<2 || n>MAXARRAY);

	for (i = 0; i < n; i++) {
		printf("座標%d x:", i + 1); scanf("%lf", &ary[i].x);
		printf("座標%d y:", i + 1); scanf("%lf", &ary[i].y);
	}
	printf("距離:%f\n", maxdistance(ary, n));
	return 0;
}