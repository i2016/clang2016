#include<stdio.h>
#include<math.h>
typedef struct point {
	double cx, cy;
}Point;

double calc_distance(Point a, Point b) {
	double sum = 0;
	sum += (a.cx - b.cx)*(a.cx - b.cx);
	sum += (a.cy - b.cy)*(a.cy - b.cy);
	return sqrt(sum);
}

int main(void) {
	Point a, b;
	a.cx = 5;
	a.cy = 14.5;
	b.cx = -3;
	b.cy = 8.5;
	printf("点Aの座標:\n");
	printf("x?\n");
	printf("%4.1f\n",a.cx);
	printf("%4.1f\n", a.cy);
	printf("y?\n");
	printf("%4.1f\n",b.cx);
	printf("%4.1f\n", b.cy);
	printf("2点A,Bの距離は%4.1fである\n", calc_distance(a, b));

	return 0;

}