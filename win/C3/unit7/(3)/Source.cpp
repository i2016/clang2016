#include<stdio.h>
#include<stdlib.h>

int main(void) {
	char name[20], tel[15];
	int age;
	FILE *f;
	if ((f = fopen("mydata.dat", "r")) == NULL) {
		printf("file ope error");
		exit(1);
	}
	fscanf(f, "%s%d%s", name, &age, tel);
	printf("%s %d %s\n", name, age, tel);
	fclose(f);
	return 0;
}