#include<stdio.h>
#include<stdlib.h>

int main(void) {
	int ch;
	FILE *fp;
	if ((fp = fopen("Source.cpp", "r")) == NULL) {
		printf("error");
		exit(1);
	}
	while ((ch = fgetc(fp)) != EOF) {
		putchar(ch);
	}
	fclose(fp);
	return 0;
}