#include<stdio.h>
double heikin(int a, int b);

struct date
{
	char name[11];
	int math, eng;
};
int main(void) {
	
	struct date a[5] = { { "小林",84,62 }, { "鈴木",70,94 },  { "高橋",65,52 }, { "田中",96,82 },{ "山田",82,77 } };
	int i;
	printf("名前　数学　英語 合計　平均\n");
	for (i = 0; i < 5; i++) {
		printf("%s  %d  %d  %d  %3.1f\n", a[i].name, a[i].math, a[i].eng, a[i].math + a[i].eng, heikin(a[i].math, a[i].eng));
	}
	return 0;
}
double heikin(int a, int b) {
	double heikin;
	heikin = (a + b) / 2.0;
	return heikin;
}

